# Word Counter #

### How do I get set up? ###

* This project has been developed in Java using version 8
* It's build tool is Apache Maven
* Packages include Main and Test
* Application code can found under main folder
* Unit test cases can be found under test folder
* Once code is checked out can be run from command line where Apache Maven has already been installed and configured
* Goto the root folder of the application and using command line and issue the command [ mvn clean install]

## Description ##
This wordcounter application has got the following functionalities in WordCounter class.

### Functionalities ###
*	Method that allows you to add words
*	Method that returns the count of how many times a given word was added to the word counter
*   Input validation while adding new word
*   Translation of word from other languages

### Tests include the following test cases ###
All test cases are defined in WordCounterTest class 
* throwExceptionOnAddWordWithInputHavingNonAlphabeticCharacterTest() covers 
[ should NOT allow addition of words with non-alphabetic characters ]   
* addTranslatedWordTest() covers [ should treat same words written in different languages as the same word, for example if
adding "flower", "flor" (Spanish word for flower) and "blume" (German word for flower) the counting method should return 3. ] 

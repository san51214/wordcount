package org.test.wordcount.util;

import org.junit.Rule;
import org.junit.rules.ExpectedException;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.test.wordcount.WordCounter;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

public class WordCounterTest {

    @org.junit.Test
    public void countAllWordsTest() {
        WordCounter wordCounter = new WordCounter();
        wordCounter.addWord("one");
        wordCounter.addWord("one");
        wordCounter.addWord("one");
        wordCounter.addWord("one");
        assertEquals(4,wordCounter.countAllWord());
    }

    @org.junit.Test
    public void countAllUniqueWords() {
        WordCounter wordCounter = new WordCounter();
        wordCounter.addWord("one");
        wordCounter.addWord("one");
        wordCounter.addWord("one");
        wordCounter.addWord("one");
        wordCounter.addWord("two");
        wordCounter.addWord("two");
        wordCounter.addWord("two");
        wordCounter.addWord("two");
        wordCounter.addWord("two");
        wordCounter.addWord("three");
        assertEquals(3,wordCounter.countAllUniqueWords());
    }

    @org.junit.Test
    public void countOccurancesOfWordTest() {
        WordCounter wordCounter = new WordCounter();
        wordCounter.addWord("one");
        wordCounter.addWord("one");
        wordCounter.addWord("one");
        wordCounter.addWord("one");
        wordCounter.addWord("two");
        wordCounter.addWord("two");
        wordCounter.addWord("two");
        wordCounter.addWord("two");
        wordCounter.addWord("two");
        wordCounter.addWord("three");
        assertEquals(4,wordCounter.getOccurances("one"));
        assertEquals(0,wordCounter.getOccurances("four"));
    }

    @Rule
    public ExpectedException exceptionRule = ExpectedException.none();

    @org.junit.Test
    public void throwExceptionOnAddWordWithInputHavingNonAlphabeticCharacterTest() {
        String word = "one!";
        String errorMessage = word+" input contains non-alphabetic character";
        exceptionRule.expect(IllegalArgumentException.class);
        exceptionRule.expectMessage(errorMessage);
        WordCounter wordCounter = new WordCounter();
        wordCounter.addWord(word);
    }

    @Mock
    WordTranslator wordTranslator;
    @Rule public MockitoRule mockitoRule = MockitoJUnit.rule();

    @org.junit.Test
    public void addTranslatedWordTest(){
       when(wordTranslator.translate("Spanish","flor")).thenReturn("flower");
       when(wordTranslator.translate("German","blume")).thenReturn("flower");
       WordCounter wordCounter = new WordCounter();
       wordCounter.setWordTranslator(wordTranslator);

        wordCounter.addWord("flower");
        wordCounter.addTranslatedWord("Spanish","flor");
        wordCounter.addTranslatedWord("German","blume");

        assertEquals(3,wordCounter.countAllWord());
    }



}
package org.test.wordcount;

import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.collections4.multimap.ArrayListValuedHashMap;
import org.test.wordcount.util.WordTranslator;

public class WordCounter {

    private MultiValuedMap<String, String> wordCollectionsMap = new ArrayListValuedHashMap();
    private String onlyAlphabetsRegex = "^[a-zA-Z]*$";
    private WordTranslator wordTranslator;

    public void addWord(String word){
        checkInputValid(word);
        wordCollectionsMap.put(word,word);
    }

    private void checkInputValid(String word){
        if(!word.matches(onlyAlphabetsRegex)){
            throw new IllegalArgumentException(word+" input contains non-alphabetic character");
        }
    }

    public int getOccurances(String word){
        if(wordCollectionsMap.containsKey(word)){
            return wordCollectionsMap.get(word).size();
        }
        return 0;
    }

    public int countAllWord(){
        return wordCollectionsMap.size();
    }

    public int countAllUniqueWords(){
        return wordCollectionsMap.keySet().size();
    }

    public void addTranslatedWord(String language,String word){
        addWord(getWordTranslator().translate(language,word));
    }

    public WordTranslator getWordTranslator() {
        return wordTranslator;
    }

    public void setWordTranslator(WordTranslator wordTranslator) {
        this.wordTranslator = wordTranslator;
    }
}

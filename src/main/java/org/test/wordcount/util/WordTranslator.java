package org.test.wordcount.util;

public interface WordTranslator {
    String translate(String language, String word);
}
